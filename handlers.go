package main

import (
	"github.com/dgrijalva/jwt-go"
)

var jwtKey = []byte("my_secret_key")

var users = map[string]string{
	"user1": "password1",
	"user2": "password2",
}

var authMethod = "Bearer"

// Create a struct that models the structure of a user, both in the request body, and in the DB
type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}
type LoginResponse struct {
	AccessToken  string `json:"AccessToken"`
	ExpiresIn    int64  `json:"ExpiresIn"`
	TokenType    string `json:"TokenType"`
	RefreshToken string `json:"RefreshToken"`
	IDToken      string `json:"IdToken"`
}

type ErrResultMessage struct {
	Body    string `json:"body,omitempty"`
	Headers struct {
		Content_Type string `json:"Content-Type,omitempty"`
	} `json:"headers,omitempty"`
	StatusCode int64 `json:"statusCode,omitempty"`
}

func SetErrResultMessage(statuscode int64, body string) ErrResultMessage {

	message := new(ErrResultMessage)

	message.Body = body
	message.StatusCode = statuscode
	message.Headers.Content_Type = "application/json "
	return *message
}
