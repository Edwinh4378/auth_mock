package main

import (
	etcddata "bitbucket.org/Edwinh4378/etcd-data"
)

type AddAnnotationRequest struct {
	RequestingRailway string                    `json:"requestingRailway"`
	Carrier           string                    `json:"carrier"`
	Allocator         string                    `json:"allocator"`
	MessageReference  string                    `json:"messageReference"`
	BookingReference  etcddata.BookingReference `json:"bookingReference"`
	Annotation        etcddata.Annotation       `json:"annotation"`
}
