package main

import (
	"fmt"

	"bitbucket.org/Edwinh4378/auth_mock/internal/rabbit"
	"github.com/streadway/amqp"
)

var connn rabbit.Conn

func rabbitmain() {
	conn, err := rabbit.GetConn("amqp://user:password@172.17.0.4")
	if err != nil {

		panic(err)
	}
	connn = conn
	// go func() {
	// 	for {
	// 		time.Sleep(time.Second)
	// 		conn.Publish("test-key", []byte(`{"message":"test"}`))
	// 	}
	// }()

	// err = connn.StartConsumer("test-queue", "test-key", handler, 2)

	// if err != nil {
	// 	panic(err)
	// }

	forever := make(chan bool)
	<-forever
}

func handler(d amqp.Delivery) bool {
	if d.Body == nil {
		fmt.Println("Error, no message body!")
		return false
	}
	fmt.Println(string(d.Body))
	return true
}
