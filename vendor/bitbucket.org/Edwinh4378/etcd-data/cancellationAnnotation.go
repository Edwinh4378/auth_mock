package etcddata

type CancellationAnnotation struct {
	ID           string       `json:"Id,omitempty"` //annotationID
	Cancellation Cancellation `json:"Cancellation,omitempty"`
	Id           string       `json:"id,omitempty"` //extrenalid
}
type Cancellation struct {
	CancellationTime string `json:"CancellationTime,omitempty"`
}
