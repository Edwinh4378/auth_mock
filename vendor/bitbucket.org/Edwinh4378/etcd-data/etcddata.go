package etcddata

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"time"
)

type ReqRetrieveTicket struct {
	RequestingRailway string                  `json:"requestingRailway"`
	Carrier           string                  `json:"carrier"` //weg??
	BookingReference  GeneralBookingReference `json:"bookingReference"`
}
type GeneralBookingReference struct {
	Allocator string `json:"Allocator"`
	BookingID string `json:"BookingID"`
	// ###### Fields not used yet
	// DepartureDate time.Time `json:"DepartureDate"`
	// IssuingDate time.Time `json:"IssuingDate"`
	// SystemCode string `json:"SystemCode"`
	// Train string `json:"Train"`
	// AccountingId string `json:"AccountingId"`
}

type TicketMessage struct {
	RequestedBookingReference BookingReference `json:"RequestedBookingReference"`
	RequestingRailway         string           `json:"RequestingRailway"`
	// SeqNb        int     `json:"seqNb"`
	// ID           string  `json:"id"`
	// DeliveryTime string  `json:"deliveryTime"`

	Ticket Ticket2 `json:"Ticket"`
}

type TicketMessageQue struct {
	// RequestedBookingReference BookingReference `json:"RequestedBookingReference"`
	// RequestingRailway         string           `json:"RequestingRailway"`
	SeqNb        int    `json:"seqNb"`
	ID           string `json:"id"`
	DeliveryTime string `json:"deliveryTime"`

	Ticket Ticket2 `json:"Ticket"`
}

type SignedAnonymousTicketData struct {
	Header  Header `json:"header"`
	Payload string `json:"payload"`
}
type Ticketdata struct {
	SignedAnonymousTicketData SignedAnonymousTicketData `json:"signedAnonymousTicketData"`
}
type Ticket struct {
	TicketData  Ticketdata  `json:"ticketData"`
	Annotations Annotations `json:"annotations,omitempty"`
}

type Ticket2 struct {
	TicketData Ticketdata   `json:"ticketData"`
	Annotation []Annotation `json:"annotations"`
}

type Header struct {
	Alg  string `json:"alg"`
	Type string `json:"type"`
}

type Annotations struct {
	Annotation []Annotation `json:"annotation"`
}

type Annotation struct {
	Header  Header `json:"header"`
	Payload string `json:"payload"`
}

type BookingReference struct {
	BookingID     string     `json:"BookingId,omitempty"`
	DepartureDate *time.Time `json:"DepartureDate,omitempty"`
	Train         string     `json:"Train,omitempty"`
}

// Ticket Payload
type CTicket struct {
	IssuingDetails  IssuingDetails  `json:"IssuingDetails"`
	TicketReference TicketReference `json:"TicketReference"`
	Annotations     []Annotation
}
type TicketPayload struct {
	IssuingDetails  IssuingDetails  `json:"IssuingDetails,omitempty"`
	TicketReference TicketReference `json:"TicketReference,omitempty"`
}
type IssuingDetails struct {
	IssuingDate time.Time `json:"IssuingDate,omitempty"`
	Issuer      string    `json:"Issuer,omitempty"`
}
type TicketReference struct {
	BookingID     string    `json:"BookingId,omitempty"`
	DepartureDate time.Time `json:"DepatureDate,omitempty"`
}

type ResRetrieveTicket struct {
	RequestedBookingReference BookingReference `json:"RequestedBookingReference"`
	RequestingRailway         string           `json:"RequestingRailway"`
	// SeqNb        int     `json:"seqNb"`
	// ID           string  `json:"id"`
	// DeliveryTime string  `json:"deliveryTime"`

	Ticket Ticket2 `json:"Ticket"`
}

type AddAnnotationRequest struct {
	RequestingRailway string           `json:"requestingRailway"`
	Carrier           string           `json:"carrier"`
	Allocator         string           `json:"allocator"`
	MessageReference  string           `json:"messageReference"`
	BookingReference  BookingReference `json:"bookingReference"`
	Annotation        Annotation       `json:"annotation"`
}

func ReadTicketPayload(base64String string) *TicketPayload {
	Payload := TicketPayload{}

	TicketPayloadJSON, err := base64.StdEncoding.DecodeString(base64String)
	if err != nil {
		fmt.Println("error:", err)
		return nil
	}

	fmt.Println(string(TicketPayloadJSON))

	err = json.Unmarshal(TicketPayloadJSON, &Payload)

	// return &Payload
	return &Payload
}

func matchAnnotation(m, stringPayload string) bool {
	Match1, err := regexp.MatchString(m, stringPayload)
	if err != nil {
		fmt.Println(err)
	}
	return Match1
}

func ReadAnnotationPayload(base64String string) (interface{}, string) {

	PayloadJSON, err := base64.StdEncoding.DecodeString(base64String)
	if err != nil {
		fmt.Println("error:", err)
	}

	switch {
	//Control
	case matchAnnotation(`"Control":`, string(PayloadJSON)):

		Annotation := ControlAnnotation{}
		err = json.Unmarshal(PayloadJSON, &Annotation)

		return &Annotation, "Control"

	//Cancellation
	case matchAnnotation(`"Cancellation":`, string(PayloadJSON)):

		Annotation := CancellationAnnotation{}
		err = json.Unmarshal(PayloadJSON, &Annotation)

		return &Annotation, "Cancellation"

	default:
		return nil, "UNKNOWN"
	}

}
func ReadTicketMessage(JSONbody []byte) *TicketMessage {
	Ticket := TicketMessage{}

	err := json.Unmarshal(JSONbody, &Ticket)
	if err != nil {
		fmt.Println(err)
	}

	return &Ticket
}
