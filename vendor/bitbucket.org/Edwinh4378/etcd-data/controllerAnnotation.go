package etcddata

type Controller struct {
	Carrier    string `json:"Carrier,omitempty"`
	DeviceID   string `json:"DeviceId,omitempty"`
	DeviceType string `json:"DeviceType,omitempty"`
	TCO        string `json:"TCO,omitempty"`
}
