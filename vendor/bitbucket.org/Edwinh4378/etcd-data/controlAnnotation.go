package etcddata

type ControlAnnotation struct {
	ID          string     `json:"Id,omitempty"` //annotationID
	Controller  Controller `json:"Controller,omitempty"`
	Location    Location   `json:"Location,omitempty"`
	Control     Control    `json:"Control,omitempty"`
	Timecreated string     `json:"Timecreated,omitempty"`
	Id          string     `json:"id,omitempty"` //external id
}

type Control struct {
	Result string `json:"Result,omitempty"`
}
