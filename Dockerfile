FROM alpine:3.12

RUN mkdir /app
WORKDIR /app
COPY . .

RUN ln -sf /usr/share/zoneinfo/CET /etc/localtime s
ENTRYPOINT  ./auth_mock -portnumber=9999 -https=false -mockactivity=false 
EXPOSE 9999