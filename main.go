package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	servicebus "github.com/Azure/azure-service-bus-go"
)

var testTickets []*etcddata.CTicket
var testTicketsMap = make(map[string]*etcddata.CTicket)

var portnumber = flag.Int("portnumber", 8000, "portnumber as int, standard port is 8000")
var filename = flag.String("testtickets", "testtickets.json", "file with testtickets in json format--testtickets.json")
var https = flag.Bool("https", false, "https true, http false; when https make sure key files are in root (cert/ke); stand is http")
var mockactivity = flag.Bool("mockactivity", true, "add a control annotation add every retrieveticket request, standard is true")
var serviceBusConnectionString = flag.String("sbus", "", "service bus connectionstring")
var etcdqueue = flag.String("q", "addticket.etcd", "queue name posting new or updated tickets")

var timeformat string = `2006-01-02T15:04:05.000Z07:00`

var sbq servicebus.Queue

func loadTestticketsJSON(filename string) {
	// Open our jsonFile
	// filename := "testtickets.json"
	jsonFile, err := os.Open(filename)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully Opened ", filename)
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &testTickets)

	fmt.Println("Amount testtickets found : ", len(testTickets))

	for _, item := range testTickets {
		ticketIdentifier := fmt.Sprint(item.IssuingDetails.Issuer, "_", item.TicketReference.BookingID)
		item.TicketReference.DepartureDate = item.IssuingDetails.IssuingDate
		testTicketsMap[ticketIdentifier] = item
	}
	fmt.Println(testTicketsMap)

}

func sendToQueue(ticketIdentifier, payload string) {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := sbq.Send(ctx, servicebus.NewMessageFromString(payload))
	if err != nil {
		fmt.Println("FATAL: ", err)
		return
	}
	fmt.Println("Done Sending to queue : ", ticketIdentifier)
	// err = q.ReceiveOne(
	// 	ctx,
	// 	servicebus.HandlerFunc(func(ctx context.Context, message *servicebus.Message) error {
	// 		fmt.Println(string(message.Data))
	// 		return message.Complete(ctx)
	// 	}))
	// if err != nil {
	// 	fmt.Println("FATAL: ", err)
	// 	return
	// }

}

func main() {

	flag.Parse()
	port := strconv.Itoa(*portnumber)

	fmt.Println("Portnumber : ", port)
	fmt.Println("mockactivity : ", *mockactivity)
	fmt.Println("etcdqueue : ", *etcdqueue)

	// go rabbitmain()

	if *serviceBusConnectionString == "" {
		queFromEnv := os.Getenv("SERVICEBUS_CONNECTION_STRING")
		if queFromEnv == "" {
			fmt.Println("servicebus connection string is not set (ENV - SERVICEBUS_CONNECTION_STRING or FLAG -sbus)")
		} else {
			*serviceBusConnectionString = queFromEnv

		}

	}

	fmt.Println("Service Bus Connection string is present : ", (*serviceBusConnectionString != ""))

	// setTestTicketdata()
	loadTestticketsJSON(*filename)
	//

	if *serviceBusConnectionString == "" {
		fmt.Println("FATAL: expected environment variable SERVICEBUS_CONNECTION_STRING not set")
		return
	}

	// Create a client to communicate with a Service Bus Namespace.
	ns, err := servicebus.NewNamespace(servicebus.NamespaceWithConnectionString(*serviceBusConnectionString))
	if err != nil {
		fmt.Println(err)
		return
	}

	// Create a client to communicate with the queue. (The queue must have already been created, see `QueueManager`)
	sbque, err := ns.NewQueue(*etcdqueue)
	if err != nil {
		fmt.Println("FATAL: ", err)
		return
	}
	sbq = *sbque
	fmt.Println(sbq.Name)
	//

	http.HandleFunc("/login", Login)
	http.HandleFunc("/addannotation", AddAnnotation)
	http.HandleFunc("/refresh", Refresh)
	http.HandleFunc("/retrieveticket", Retrieveticket)
	fmt.Println("Secure http : ", *https)
	if *https {
		log.Fatal(http.ListenAndServeTLS(":"+port, "cert.pem", "key.pem", nil))
	} else {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	}

}
