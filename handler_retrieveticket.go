package main

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

func Retrieveticket(w http.ResponseWriter, r *http.Request) {

	tknStr := tokenReader(w, r)

	// Initialize a new instance of `Claims`
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		fmt.Println(jwt.ErrSignatureInvalid)
		fmt.Println(err)
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !tkn.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	var request etcddata.ReqRetrieveTicket
	err = json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	//Look if ticket is present
	ticketIdentifier := fmt.Sprint(request.BookingReference.Allocator, "_", request.BookingReference.BookingID)

	storedTicket, exists := testTicketsMap[ticketIdentifier]

	if exists {

		//when mockactivity flag is true
		if *mockactivity {
			storedTicket.Annotations = append(storedTicket.Annotations, etcddata.Annotation{Header: etcddata.Header{Alg: "none", Type: "application/jose+json"}, Payload: createAnnotationPayload()})
		}

		//create ticket payload
		ticketpayload := createTicketPayload(*storedTicket)

		//create full ticket response

		result := etcddata.ResRetrieveTicket{
			RequestedBookingReference: etcddata.BookingReference{BookingID: request.BookingReference.BookingID},
			RequestingRailway:         request.RequestingRailway,
			Ticket: etcddata.Ticket2{
				TicketData: etcddata.Ticketdata{SignedAnonymousTicketData: etcddata.SignedAnonymousTicketData{
					Header:  etcddata.Header{Alg: "none", Type: "application/jose+json"},
					Payload: ticketpayload}},

				Annotation: storedTicket.Annotations},
		}

		jsonresponse, err := json.Marshal(result)
		if err != nil {
			panic(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(jsonresponse)

	} else {

		jsonresponse, err := json.Marshal(SetErrResultMessage(404, `{"message": "Ticket not found"}`))
		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(jsonresponse)
	}

}

func createTicketPayload(ticket2 etcddata.CTicket) string {
	ticketDataReStruct := struct {
		etcddata.IssuingDetails  `json:"IssuingDetails"`
		etcddata.TicketReference `json:"TicketReference"`
	}{ticket2.IssuingDetails, ticket2.TicketReference}

	ticketpayloadJSON, err := json.Marshal(ticketDataReStruct)
	if err != nil {
		panic(err)
	}

	ticketpayload := b64.StdEncoding.EncodeToString(ticketpayloadJSON)
	return ticketpayload
}

func createAnnotationPayload() string {
	newAnnotation := etcddata.ControlAnnotation{}
	newAnnotation.ID = generateUUID().String()

	newAnnotation.Timecreated = time.Now().Format(timeformat)

	newAnnotation.Controller.Carrier = "1184"
	newAnnotation.Controller.DeviceType = "SB"
	newAnnotation.Controller.DeviceID = "XXX999XXX"
	newAnnotation.Controller.TCO = "1184"
	newAnnotation.Location.OnRoute.FromStation.LocalCode = "118400058"

	newAnnotation.Control.Result = "OK"
	newAnnotation.Id = generateUUID().String()

	AnnotationPayloadJSON, err := json.Marshal(newAnnotation)
	if err != nil {
		fmt.Println("Payload marshall JSON problem")
	}
	AnnotationPayload := b64.StdEncoding.EncodeToString(AnnotationPayloadJSON)
	return AnnotationPayload
}

func generateUUID() uuid.UUID {
	uuidGenerated, err := uuid.NewRandom()

	if err != nil {
		fmt.Println("UUID problem")
		panic(err)
	}
	return uuidGenerated
}
