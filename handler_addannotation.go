package main

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	"github.com/dgrijalva/jwt-go"
)

func AddAnnotation(w http.ResponseWriter, r *http.Request) {

	tknStr := tokenReader(w, r)

	// Get the JWT string from the cookie

	// Initialize a new instance of `Claims`
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match

	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		fmt.Println(jwt.ErrSignatureInvalid)
		fmt.Println(err)
		if err == jwt.ErrSignatureInvalid {
			fmt.Println("Bearer token not valid")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !tkn.Valid {
		fmt.Println("Bearer token not valid")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	var AnnoReq AddAnnotationRequest

	err = json.NewDecoder(r.Body).Decode(&AnnoReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	foundannotationb64 := AnnoReq.Annotation.Payload

	annotationinterface, annotationType := etcddata.ReadAnnotationPayload(foundannotationb64)

	var AnnotationPayload string
	switch {
	// Control
	case annotationType == "Control":
		{
			fmt.Println("Control annotation found")
			cast := annotationinterface.(*etcddata.ControlAnnotation)
			//set external ETCD ID
			cast.Id = generateUUID().String()

			AnnotationPayloadJSON, err := json.Marshal(cast)
			if err != nil {
				fmt.Println("Payload marshall JSON problem")
			}
			AnnotationPayload = b64.StdEncoding.EncodeToString(AnnotationPayloadJSON)

		}
	case annotationType == "Cancellation":
		{
			fmt.Println("Cancellation annotation found")
			cast := annotationinterface.(*etcddata.CancellationAnnotation)
			//set external ETCD ID
			cast.Id = generateUUID().String()
			AnnotationPayloadJSON, err := json.Marshal(cast)
			if err != nil {
				fmt.Println("Payload marshall JSON problem")
			}
			AnnotationPayload = b64.StdEncoding.EncodeToString(AnnotationPayloadJSON)

		}
	}
	ticketIdentifier := fmt.Sprint(AnnoReq.Allocator, "_", AnnoReq.BookingReference.BookingID)

	storedTicket, exists := testTicketsMap[ticketIdentifier]
	if exists {
		fmt.Printf("%s exists in memory\n ", storedTicket.TicketReference.BookingID)
		storedTicket.Annotations = append(storedTicket.Annotations, etcddata.Annotation{Header: etcddata.Header{Alg: "none", Type: "application/jose+json"}, Payload: AnnotationPayload})
		fmt.Println("Ticket is known, annotation is appended to", storedTicket.TicketReference.BookingID, " ", storedTicket.IssuingDetails.Issuer)
	} else {

		createticket(ticketIdentifier, time.Now().Format(timeformat), AnnoReq.Allocator, AnnoReq.BookingReference.BookingID)
		storedTicket = testTicketsMap[ticketIdentifier]
		storedTicket.Annotations = append(storedTicket.Annotations, etcddata.Annotation{Header: etcddata.Header{Alg: "none", Type: "application/jose+json"}, Payload: AnnotationPayload})
		fmt.Println("Ticket is created, annotation is appended to", storedTicket.TicketReference.BookingID, " ", storedTicket.IssuingDetails.Issuer)
	}

	ticketpayload := createTicketPayload(*storedTicket)

	qeueuMessage := etcddata.TicketMessageQue{
		SeqNb:        1,
		ID:           "undefined",
		DeliveryTime: time.Now().Format(timeformat),
		Ticket: etcddata.Ticket2{
			TicketData: etcddata.Ticketdata{SignedAnonymousTicketData: etcddata.SignedAnonymousTicketData{Header: etcddata.Header{Alg: "none", Type: "application/jose+json"}, Payload: ticketpayload}},
			Annotation: storedTicket.Annotations,
		}}

	jsonresponse, err := json.Marshal(qeueuMessage)

	// post to etcd queque

	fmt.Println("post to queue, ticketID :", ticketIdentifier)
	// fmt.Println(string(jsonresponse))

	go sendToQueue(ticketIdentifier, string(jsonresponse))

}

func createticket(ticketIdentifier, IssuingDate, Issuer, BookingID string) {
	tt := new(etcddata.CTicket)

	t, _ := time.Parse(timeformat, IssuingDate) // "2020-02-26T14:47:03.54+00:00"

	tt.IssuingDetails.IssuingDate = t
	tt.IssuingDetails.Issuer = Issuer        //"0080"
	tt.TicketReference.BookingID = BookingID //"TMRSCJN-1"
	tt.TicketReference.DepartureDate = t

	testTicketsMap[ticketIdentifier] = tt
}

func ticketKnown(slice []*etcddata.CTicket, ticketid string, rics string) (int, bool) {
	for i, item := range slice {
		if item.IssuingDetails.Issuer == rics && item.TicketReference.BookingID == ticketid {
			return i, true
		}
	}
	return -1, false
}

func tokenReader(w http.ResponseWriter, r *http.Request) string {

	var tknStr2 string
	switch {
	//###########BEARER###########
	case authMethod == "Bearer":
		bearToken := r.Header.Get("Authorization")

		if bearToken == "" {
			fmt.Println("Token not found in Header: Authorization")
			return ""
			// w.WriteHeader(http.StatusBadRequest)

		}

		strArr := strings.Split(bearToken, " ")
		// check if token is a bearer token and is in the correct format
		if len(strArr) == 2 && (strArr[0] == "Bearer") {
			tknStr2 = strArr[1]
			// fmt.Println("Token is found and is in correct bearer format")
		} else {
			fmt.Printf("A token is found and but not in correct bearer format : %v\n", bearToken)

		}
	//###########COOKIE###########
	case authMethod == "COOKIE":
		c, err := r.Cookie("token")

		if err != nil {
			if err == http.ErrNoCookie {
				// If the cookie is not set, return an unauthorized status
				w.WriteHeader(http.StatusUnauthorized)
				return ""
			}
			// For any other type of error, return a bad request status
			w.WriteHeader(http.StatusBadRequest)
			return ""
		}
		tknStr2 = c.Value
	}
	return tknStr2
}
