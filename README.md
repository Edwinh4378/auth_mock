# ETCD add annotation MOCK
Startup values

-portnumber standard port is 8000 (... -portnumber 8999)

## endpoints
##  ../login 
--> login with username and password, to receive access tokens in json format

### Request:
POST

```
{
    "username": "user1",
    "password": "password1"
}
```

### Result example:
```
{
    "AccessToken": "DUMMY_ACCESS",
    "ExpiresIn": 1599818462,
    "TokenType": "Bearer",
    "RefreshToken": "DUMMY_REFRESH",
    "IdToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiZXhwIjoxNTk5ODE4NDYyfQ.RUzUGdy8leb_6MWjd9t7A8NiHVRDi6_FQqlNPO4Q1lI"
}
```
Use IdToken as bearer token (JWT) as authentication
## ../addannotation
--> sends annotation to etcd

### Request:
POST

authentication: use token in header authentication: "Bearer \<token>"

```
{
    "requestingRailway": "1184",
    "carrier": "1184",
    "allocator": "0080",
    "messageReference": "1e1806c0-991e-45a3-b7e2-c6eee008c4e2",
    "bookingReference": {
        "BookingId": "TMRSCJN-1",
        "DepartureDate": "2019-12-17T10:41:15.46+00:00",
        "Train": "0"
    },
    "annotation": {
        "header": {
            "alg": "none",
            "type": "application/jose+json"
        },
        "payload": "eyJJZCI6ImFhMjhiOGE3LWRiMWQtNDgyYy05MTJkLWZhMGM1NGFkZmExZSIsIkNvbnRyb2xsZXIiOnsiQ2FycmllciI6IjExODQiLCJEZXZpY2VJZCI6IlhYWDk5OVhYWCIsIkRldmljZVR5cGUiOiJTQiIsIlRDTyI6IjExODQifSwiTG9jYXRpb24iOnsiT25Sb3V0ZSI6eyJGcm9tU3RhdGlvbiI6eyJMb2NhbENvZGUiOiIxMTg0MDAwNTgifX19LCJDb250cm9sIjp7IlJlc3VsdCI6Ik9LIn0sIlRpbWVjcmVhdGVkIjoiMjAyMC0xMC0wOFQxMjozNzo1OSswMjowMCJ9"
    }
}
```

## retrieveticket
--> retrieves ticketdata from etcd,

MOCK: every call generates a new annotation for the retrieved ticket.
### Request:
POST

authentication: use token in header authentication: "Bearer \<token>"

```
{
    "RequestingRailway": "1184",
    "Carrier": "1184",
    "BookingReference": {
        "Allocator": "0080",
        "BookingId": "TMRSCJN-1"
    }
}
```
### Result example:

```
{
    "RequestedBookingReference": {
        "BookingId": "TMRSCJN-1"
    },
    "RequestingRailway": "1184",
    "Ticket": {
        "ticketData": {
            "signedAnonymousTicketData": {
                "header": {
                    "alg": "none",
                    "type": "application/jose+json"
                },
                "payload": "eyJJc3N1aW5nRGV0YWlscyI6eyJJc3N1aW5nRGF0ZSI6IjIwMjAtMDItMjZUMTQ6NDc6MDMuNTRaIiwiSXNzdWVyIjoiMDA4MCJ9LCJUaWNrZXRSZWZlcmVuY2UiOnsiQm9va2luZ0lkIjoiVE1SU0NKTi0xIn19"
            }
        },
        "annotations": [
            {
                "header": {
                    "alg": "none",
                    "type": "application/jose+json"
                },
                "payload": "eyJJZCI6IjQyMzE4YTBlLTRjNTYtNDUyMy05Nzc4LTZmMGM3MTViMDRjNyIsIkNvbnRyb2xsZXIiOnsiQ2FycmllciI6IjExODQiLCJEZXZpY2VJZCI6IlhYWDk5OVhYWCIsIkRldmljZVR5cGUiOiJTQiIsIlRDTyI6IjExODQifSwiTG9jYXRpb24iOnsiT25Sb3V0ZSI6eyJGcm9tU3RhdGlvbiI6eyJMb2NhbENvZGUiOiIxMTg0MDAwNTgifX19LCJDb250cm9sIjp7IlJlc3VsdCI6Ik9LIn0sIlRpbWVjcmVhdGVkIjoiMjAyMC0xMC0xMVQwMTowNDo1NCswMjowMCJ9"
            }
        ]
    }
}
```



todo 
* add annotation to existing testticket (bypass autogeneration annotation when retrieving )
* time notation problem zone 00:00 is written as "z" in go but etcd serves 00:00 (should there be timezone info in etcd times?)
* validate json

Done
* parse base64 annotation info
*  check json


## ../refresh
not used only when cookies are enabled (not bearer token)